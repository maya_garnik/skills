var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: './app/js/index.js',
    output: {
        devtoolLineToLine: true,
        sourceMapFilename: "./js/app.js.map",
        pathinfo: true,
        path: __dirname,
        publicPath: 'http://localhost:8080/source',
        filename: './dist/js/app.js'
    },
    module: {
        loaders: [
            { 
                test: /\.scss$/, 
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader!postcss-loader') },
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react', 'stage-0']
                }
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    plugins: [
        new ExtractTextPlugin('dist/css/[name].css', {
            allChunks: true
        }), 
        new webpack.optimize.UglifyJsPlugin({
            include: /\.min\.js$/,
            minimize: true
        }),
        new webpack.DefinePlugin({
          "process.env": { 
             NODE_ENV: JSON.stringify("development") 
           }
        })
    ]
};