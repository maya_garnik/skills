'use strict'
import React from 'react';
import ValidateFields from './../ValidateFields';
import Title from './../Title';
import FormFieldsStyle from './FormFields.scss';

export default class FormFields extends React.Component{
	componentDidMount() {
		window.addEventListener('checkOnEmpty',() => this.checkOnEmpty());
	}
  	checkOnEmpty() {
  		let data = this.props.newProp;
  		if (data.steps[data.stepIndex]) {
			data.steps[data.stepIndex].fields.forEach((item, index) => {
				if (item.value === '') {
					if (this.refs[item.name]) {
						if (data.nextClickState) {
							ValidateFields.checkState(
								this.refs[data.steps[data.stepIndex].fields[index].name].parentNode, 
								data.steps[data.stepIndex].fields[index], 
								ValidateFields.validate(data.steps[data.stepIndex].fields).invalidFields[index]
							);
						}
					}
				}
			});
  		}
  	}
    setValidState(accessAge) {
    	let data = this.props.newProp;
    	let validateArr = data.validState;
    	let tempSteps = data.steps;
    	let stepIndex = data.stepIndex;
    	if (tempSteps) {
	    	tempSteps.forEach((obj,i) => {
				if (!obj.ckeckAge) {
					obj.disable = false;
				} else {
					if (accessAge) {
						switch (obj.ckeckAge.rules) {
							case 'less':
								obj.disable = !(parseInt(obj.ckeckAge.age,10) >= accessAge);
								break;
							case 'more':
								obj.disable = !(parseInt(obj.ckeckAge.age,10) < accessAge);
								break;
						}
					}
				}	
	    	});
	  		validateArr[stepIndex] = { 'isValid' : ValidateFields.validate(tempSteps[stepIndex].fields).validState };
	  		return	 {
			  			validState: validateArr,
			  			steps: tempSteps
	  				};
    	}
    }
    handleChange(e, stepIndex , fieldIndex) {
    	let steps = this.props.newProp.steps;
  		if (e) {
	  		steps[stepIndex].fields[fieldIndex].value = e.target.value;
	  		steps[stepIndex].fields[fieldIndex].fieldInitialValue = e.target.placeholder;
  		}
		this.setState({steps:steps }, function() {
			this.setState(this.setValidState());
		});
		if (steps[stepIndex].fields[fieldIndex].name === 'age') {
			this.setState(this.setValidState(e.target.value));
		}

		ValidateFields.checkState(
			this.refs[steps[stepIndex].fields[fieldIndex].name].parentNode, 
			steps[stepIndex].fields[fieldIndex], 
			ValidateFields.validate(steps[stepIndex].fields).invalidFields[fieldIndex]
		);
    }
    render(){
    	let steps = this.props.newProp.steps;
    	let stepIndex = this.props.newProp.stepIndex;
		let myEvet = new CustomEvent('CustomEvent');
		myEvet.initEvent('checkOnEmpty', true, true);
		window.dispatchEvent(myEvet);
    	return(
    		<div>
	    		{
    			(steps.length) && 
				steps.map((obj,i) => {
	  				if (stepIndex === i) {
	  					return (
	  						<div key={i}>
	  							<Title title={obj.title} />
			  					{obj.fields.map((field,j) => {
			  						return (
			  							<div key={j} className={FormFieldsStyle.field}>
				  							<label htmlFor={j}>{field.label}</label>
				  							<input 
				  								id={j} 
				  								type={field.fieldType} 
				  								className={field.validationClass} 
				  								name={field.name}
				  								ref={field.name} 
				  								onChange={(e) => this.handleChange(e, i, j)} 
				  								placeholder={field.value} 
				  								value={steps[stepIndex].fields[j].value} 
				  							/>
			  							</div>
			  						)
								})}
								<p>{obj.textInfo}</p>
	  						</div>
						)
	  				}
	  			})
	    		}
    		</div>
    	)
    }
}