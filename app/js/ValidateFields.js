'use strict';
import FormFieldsStyle from './FormFields/FormFields.scss';
class ValidateFields {
  	validate(fields) {
		const regEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		const regNumber = /^[0-9]+$/;
  		let passwordMatch = [];

		passwordMatch = fields.filter((field) => {
			 return field.validationClass === 'required-password';
		});

		let matchFlag = true;
		passwordMatch.forEach((item, index) => {
			if (index > 0 && passwordMatch[index-1].value !== item.value) {
				matchFlag = false;
				return false;
			}
		});
		let tempValid = true;
		let validateArr = this.validateState;
		let invalidFields = [];
		let curState;
  		fields.map((field, ind) => {
  			if (typeof field.value !== 'undefined') {
					switch (field.validationClass) {
						case 'required':
							curState = this.checkState(null, field, !field.value.length || field.value === field.fieldInitialValue);
							if (!curState){
								tempValid = false;
							}
							invalidFields.push(!curState);
							break;
						case 'required-password':
							curState = this.checkState(null, field, !field.value.length || !matchFlag);
							if (!curState){
								tempValid = false;
							}
							invalidFields.push(!curState);
							break;
						case 'required-email':
							curState = this.checkState(null, field, !regEmail.test(field.value));
							if (!curState){
								tempValid = false;
							}
							invalidFields.push(!curState);
							break;
						case 'required-number':
							curState = this.checkState(null, field, !regNumber.test(field.value));
							if (!curState){
								tempValid = false;
							}
							invalidFields.push(!curState);
							break;
						case 'checkbox':
							if (!this.checkState(null, field, !field.is(':checked'))){
								tempValid = false;
							}
							break;
					}
  			}
  		});
		return {
	  			validState: tempValid,
	  			invalidFields: invalidFields
  			};
  	}
  	checkState(node ,field, error) {

  		if(node !== null) {
  			node.className = node.className.replace(FormFieldsStyle.success, '');
  			node.className = node.className.replace(FormFieldsStyle.error, '');
			if (field.mainPasswordField) {
				if (field.value === '') {
					node.className+= ' ' + FormFieldsStyle.error;
				} else {
					node.className+=' ' + FormFieldsStyle.success;
				}
			} else {
				if(error) {
					node.className+=' ' + FormFieldsStyle.error;
				} else {
					node.className+=' ' + FormFieldsStyle.success;
				}
			}
  		}
		if(!error) {
			return true;
		}
  	}
}

export default new ValidateFields();