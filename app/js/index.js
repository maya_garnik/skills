'use strict';
import jQuery from 'jQuery';
import React from 'react';
import ReactDOM from 'react-dom';
import CreateForm from './CreateForm';
import NavButtons from './NaigationButtons/NavigationButtons';
import LabelButtons from './LabelButtons/LabelButtons';

let serverRequest = function(source) {
	jQuery.ajax(source).then(
		(data) => {
			this.onUpdate({steps: data.steps});
			let validArr = data.steps.map((item, index) => {
				item.disable = (index !== 0);
				return { 'isValid' :item.isValid}; 
			});
			this.onUpdate({validState: validArr});
		},
		(xhr, status, err) => {
			console.error(this.props.url, status, err.toString());
		}
	);
};

class Wizard extends React.Component{
    state = {
		steps:[],
		validState:[],
		stepIndex: 0,
		stepValidationState: false,
		accessAge: false,
		lastStep: false,
		nextClickState: false
    }
    componentDidMount() {
    	serverRequest.apply(this, [this.props.source]);
    }
	onUpdate(obj){
		this.setState(obj);
	}
	render() {
		const {stepIndex} = this.state;
		return (
			<div className="steps">
				{
					(this.state.steps.length) &&  
					<LabelButtons {...{newProp: this.state}} onUpdate={this.onUpdate.bind(this)} />
				}
				<div>
					<CreateForm {...{newProp: this.state}}/>
					<NavButtons {...{newProp: this.state }} onUpdate={this.onUpdate.bind(this)}
					/>
				</div>
			</div>
		);
	}
}

ReactDOM.render(
	<Wizard source="../app/source/steps.json"/>,
		document.getElementById('wrapper')
	);