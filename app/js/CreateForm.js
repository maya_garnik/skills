'use strict'
import React from 'react';
import ValidateFields from './ValidateFields';
import FormFields from './FormFields/FormFields';

export default class CreateForm extends React.Component{
 	static propTypes = {
	    successMessage: React.PropTypes.string.isRequired
	}
	static defaultProps = {
	    successMessage: 'Submitted'
	}
	componentDidMount() {
     window.addEventListener('successMessage',() => this.successMessage());
	}
  	successMessage() {
  		let data = this.props.newProp;
  		if (data.lastStep) {
  			this.refs.wizardForm.submit();
  			alert(this.props.successMessage);
  		}
  	}
    render(){
      let myEvet = new CustomEvent('CustomEvent');
      myEvet.initEvent('successMessage', true, true);
      window.dispatchEvent(myEvet);
    	return(
    		<form ref="wizardForm">
				<FormFields {...{newProp: this.props.newProp}} />
    		</form>
    	);
    }
}