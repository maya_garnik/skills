'use strict'
import React from 'react';
import labelBtnStyles from './LabelButton.scss';

export default class LabelButtons extends React.Component{
  	labelHandler(ind) {
  		let data = this.props.newProp;
		const {stepIndex} = data;
		const {onUpdate} = this.props;
		let newInd = data.stepIndex;
		if (!data.steps[ind].disable) {
			for (let i = 0; i <= ind ; i++) {
				if ( i <= ind && data.validState[i].isValid) {
					newInd = ind;
					break;
				}
			}
			onUpdate({stepIndex: newInd});
		}
  	}
    render(){
    	let data = this.props.newProp;
    	const {stepIndex} = data;
    	return(
			<div className="buttons-area">
  			 	{data.steps.map((obj,ind) => {
  			 		let buttonclassName = ((ind === stepIndex) ? labelBtnStyles.active : '') + ' ' + labelBtnStyles.button;
					return	 (
						<button key={ind} className={buttonclassName} onClick={() => this.labelHandler(ind)}>
							<em>{ind + 1}</em>
							<span>{obj.name}</span>
						</button>
					)
				})}
		  	</div>
    	);
    }
}