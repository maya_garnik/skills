'use strict'
import React from 'react';
import navBtnStyles from './NavButtons.scss';

export default class NavButtons extends React.Component{
	buttonHandler(direction){
    	let data = this.props.newProp;
  		let steps = data.steps;
    	const {stepIndex} = data;
    	let newInd = data.stepIndex;
    	const {onUpdate} = this.props;
    	if (direction) {
	    	if (data.validState[data.stepIndex].isValid) {
	    		newInd = steps.slice(stepIndex + 1,steps.length).find((el) => !el.disable);
	    		if (newInd) {
	    			onUpdate({stepIndex: newInd.id});	
	    		}
	    	}
    	} else {
			if (stepIndex > 0) {
				newInd = steps.slice(0, stepIndex).reverse().find((el) => !el.disable);
	    	}
			if (newInd) {
				onUpdate({stepIndex: newInd.id});
			}
    	}
		if (direction) {
	    	this.props.onUpdate({nextClickState: true});
	  		if (steps.length - 1 === data.stepIndex && data.validState[data.stepIndex].isValid) {
	  			onUpdate({ lastStep: true });
	  		}
		}
	}
    render(){
    	let data = this.props.newProp;
    	const {stepIndex} = data;
    	return(
			<div className="nav-area">
				<button
						className={navBtnStyles.button + ' ' + navBtnStyles.prevBtn}
						disabled={stepIndex === 0}
						onClick={() => this.buttonHandler(0)}
						ref="prevBtn"
				>Back</button>
				<button 
						className={navBtnStyles.button + ' ' + navBtnStyles.nextBtn}
						disabled={stepIndex === 2}
						onClick={() => this.buttonHandler(1)}
						ref="nextBtn"
				>{(data.steps.length - 1 === data.stepIndex) ? 'finish' :  'next'}</button>
			</div>
    	);
    }
}