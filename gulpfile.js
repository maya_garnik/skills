var gulp = require('gulp'),
	sass = require('gulp-sass'),
	watch = require('gulp-watch'),
	webpack = require('webpack'),
	webpackConfig = require('./webpack.config.js'),
	plugins = require('gulp-load-plugins')(),
	htmlbuild = require('gulp-htmlbuild');

// Compile htnl
gulp.task('buildHtml', function () {
  gulp.src(['app/index.html'])
    .pipe(gulp.dest('./dist'));
});

// Compile  Sass
gulp.task('styles', function() {
    return gulp.src('app/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css'));
});

// Compile  script
gulp.task('scripts', function() {
	return webpack(webpackConfig, function(err, stats) {
			    console.log(err);
			});
});

gulp.task('watch', function() {
 	// Watch the sass files
	gulp.watch('./app/scss/*.scss', ['styles']); 
	// Watch the js files
	gulp.watch('./app/js/*.js', ['scripts']); 

    watch(['./app/scss/*.scss'], function() {
    	gulp.start('styles');
    });

    watch(['./app/js/**/*.js'], function() {
    	gulp.start('scripts');
    });
});

gulp.task('default', ['buildHtml', 'styles', 'scripts', 'watch']);